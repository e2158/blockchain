using Blockchain;
using Startup.Configs;

namespace Startup.Initialization;

public interface IBlockchainInitializer
{
    void Initialize(IBlockchain blockchain);
}

public class BlockchainInitializer : IBlockchainInitializer
{
    private readonly GenesisBlock _genesisBlock;

    public BlockchainInitializer(GenesisBlock genesisBlock) =>
        _genesisBlock = genesisBlock;

    public void Initialize(IBlockchain blockchain)
    {
        if (blockchain.BlocksCount == 0)
            blockchain.AddBlock(Convert(_genesisBlock));
    }

    private static Block Convert(GenesisBlock genesisBlock) =>
        new (
            genesisBlock.Height,
            genesisBlock.Creator,
            genesisBlock.Timestamp,
            genesisBlock.Hash,
            genesisBlock.PreviousHash,
            genesisBlock.Transactions);
}
