using Output;

namespace Startup.Menu.Tools;

public interface IDividerWriter
{
    void Write();
    void Write(string text);
}

public class DividerWriter : IDividerWriter
{
    private readonly IOutput _output;
    private readonly string _divider;

    public DividerWriter(IOutput output, string divider)
    {
        _output = output;
        _divider = divider;
    }

    public void Write() =>
        _output.WriteLine(_divider);

    public void Write(string text) =>
        _output.WriteLine(
            BuildTextInDivider(text.ToUpper()));

    private string BuildTextInDivider(string text)
    {
        int dividerLength = _divider.Length;
        int textLength = text.Length;

        if (textLength >= dividerLength)
            return text;

        int diff = dividerLength - textLength;
        int offset = diff / 2;

        return _divider.Insert(offset, text).Remove(dividerLength);
    }
}
