using Blockchain.Printing;

namespace Startup.Menu;

public class GenesisBlockMenu : IMenu
{
    private readonly IBlockchainPrinter _printer;

    public string Header => "Genesis Block";

    public GenesisBlockMenu(IBlockchainPrinter printer) =>
        _printer = printer;

    // TODO: Сделать вывод информации, что блока нет
    public void Handle() =>
        _printer.PrintGenesisBlock();
}
