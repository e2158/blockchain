using Blockchain.Printing;

namespace Startup.Menu;

public class LastBlockMenu : IMenu
{
    private readonly IBlockchainPrinter _printer;

    public string Header => "Last Block";

    public LastBlockMenu(IBlockchainPrinter printer) =>
        _printer = printer;

    // TODO: сделать вывод информации, что блока нет
    public void Handle() =>
        _printer.PrintLastBlock();
}
