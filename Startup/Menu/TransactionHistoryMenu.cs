using Input;
using Output;
using Blockchain.Printing;

namespace Startup.Menu;

public class TransactionHistoryMenu : IMenu
{
    private readonly IInput _input;
    private readonly IOutput _output;
    private readonly IBlockchainPrinter _printer;

    public string Header => "Transaction History";

    public TransactionHistoryMenu(IInput input, IOutput output, IBlockchainPrinter printer)
    {
        _input = input;
        _output = output;
        _printer = printer;
    }

    // TODO: сделать вывод информации, что история пуста
    public void Handle()
    {
        _output.Write("Please, enter name: ");

        string name = default!;
        while (string.IsNullOrEmpty(name))
            name = _input.ReadString()!;

        _output.WriteLine();
        _printer.PrintTransactionHistory(name);
    }
}
