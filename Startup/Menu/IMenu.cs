namespace Startup.Menu;

public interface IMenu
{
    string Header { get; }
    void Handle();
}
