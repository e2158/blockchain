using Input;
using Output;
using Blockchain;

namespace Startup.Menu;

public class AccountBalanceMenu : IMenu
{
    private readonly IInput _input;
    private readonly IOutput _output;
    private readonly ICalculator _calculator;

    public string Header => "Account Balance";

    public AccountBalanceMenu(IInput input, IOutput output, ICalculator calculator)
    {
        _input = input;
        _output = output;
        _calculator = calculator;
    }

    public void Handle()
    {
        _output.Write("Please, enter name: ");

        string name = default!;
        while (string.IsNullOrEmpty(name))
            name = _input.ReadString()!;

        double balance = _calculator.CalcBalance(name);

        _output.WriteLine();
        _output.WriteLine($"Balance: {balance}");
    }
}
