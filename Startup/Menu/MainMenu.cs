using Utils;
using Input;
using Output;
using Startup.Menu.Tools;

namespace Startup.Menu
{
    public class MainMenu : IMenu
    {
        private readonly IInput _input;
        private readonly IOutput _output;
        private readonly IDividerWriter _divider;
        private readonly IMenu[] _menus;

        public string Header => "Main Menu";

        public MainMenu(IInput input, IOutput output, IDividerWriter divider, IMenu[] menus)
        {
            _input = input;
            _output = output;
            _divider = divider;
            _menus = menus;
        }

        public void Handle()
        {
            bool handled = false;
            int counter = 0;

            do
            {
                WriteMenuItems();
                _divider.Write();

                if (counter++ > 0)
                    _output.WriteLine("Enter the number corresponding to the menu item");

                if (TrySelectMenuItem(out IMenu menuItem))
                {
                    WriteMenuItem(menuItem);
                    handled = true;
                }
            } while (!handled);

            _output.WriteLine();
            _output.WriteLine("Press any key to continue...");
            _input.ReadAnyKey();
        }

        private bool TrySelectMenuItem(out IMenu menuItem)
        {
            menuItem = null!;

            _output.Write("Select Item: ");
            int? input = _input.ReadInt();

            if (!input.HasValue)
                return false;

            int itemIndex = input.Value - 1;
            var range = new Range(0, _menus.Length - 1);

            if (!range.Contains(itemIndex))
                return false;

            menuItem = _menus[itemIndex];
            return true;
        }

        private void WriteMenuItems()
        {
            _output.Clear();
            _divider.Write(Header);

            for (int i = 0 ; i < _menus.Length; i++)
                _output.WriteLine($"{i + 1}. {_menus[i].Header}");
        }

        private void WriteMenuItem(IMenu menu)
        {
            _output.Clear();
            _divider.Write(menu.Header);
            menu.Handle();
        }
    }
}