using Input;
using Output;
using Blockchain;

namespace Startup.Menu;

public class CreateBlockMenu : IMenu
{
    private readonly IInput _input;
    private readonly IOutput _output;
    private readonly IBlockchain _blockchain;
    private readonly IBlockFactory _blockFactory;
    private readonly IDequeueableTransactionPool _transactionPool;

    public string Header => "Create Block";

    public CreateBlockMenu(IInput input, IOutput output, IBlockchain blockchain,
        IBlockFactory blockFactory, IDequeueableTransactionPool transactionPool)
    {
        _input = input;
        _output = output;
        _blockchain = blockchain;
        _blockFactory = blockFactory;
        _transactionPool = transactionPool;
    }

    public void Handle()
    {
        Transaction[] transactions = _transactionPool.DequeueTransactions().ToArray();

        if (transactions.Length == 0)
        {
            _output.WriteLine("No transactions in pool to create a block in pool. " +
                              "Please create the one first!");
            return;
        }

        _output.Write("Please, enter the creator's name: ");

        string name = default!;
        while (string.IsNullOrEmpty(name))
            name = _input.ReadString()!;

        Block lastBlock = _blockchain.LastBlock;
        Block block = _blockFactory.Create(lastBlock.Height + 1, name, lastBlock.Hash, transactions);
        _blockchain.AddBlock(block);
    }
}
