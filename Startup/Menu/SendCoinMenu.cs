using Input;
using Output;
using Blockchain;

namespace Startup.Menu;

public class SendCoinMenu : IMenu
{
    private readonly IInput _input;
    private readonly IOutput _output;
    private readonly ITransactionFactory _transactionFactory;
    private readonly ITransactionValidator _transactionValidator;
    private readonly IEnqueueableTransactionPool _transactionPool;

    public string Header => "Send Coin";

    public SendCoinMenu(IInput input, IOutput output, ITransactionFactory transactionFactory,
        ITransactionValidator transactionValidator, IEnqueueableTransactionPool transactionPool)
    {
        _input = input;
        _output = output;
        _transactionFactory = transactionFactory;
        _transactionValidator = transactionValidator;
        _transactionPool = transactionPool;
    }

    public void Handle()
    {
        _output.Write("Please, enter the sender's name: ");
        string sender = InputName();

        _output.Write("Please, enter the recipient name: ");
        string recipient = InputName();

        _output.Write("Please, enter the amount: ");
        double amount = InputAmount();

        _output.Write("Please, enter fee: ");
        double fee = InputAmount();

        _output.WriteLine();

        ValidationResult validationResult = _transactionValidator.Validate(sender, amount, fee);
        if (validationResult.IsError)
        {
            _output.WriteLine(validationResult.Error);
            return;
        }

        Transaction transaction = _transactionFactory.Create(sender, recipient, amount, fee);
        _transactionPool.EnqueueTransaction(transaction);

        _output.WriteLine("Transaction was added to transaction pool.");
    }

    private string InputName()
    {
        string name = default!;
        while (string.IsNullOrEmpty(name))
            name = _input.ReadString()!;

        return name;
    }

    private double InputAmount()
    {
        double? amount = default!;
        while (!amount.HasValue)
            amount = _input.ReadDouble();

        return amount.Value;
    }
}
