using Blockchain.Printing;

namespace Startup.Menu;

public class BlockchainMenu : IMenu
{
    private readonly IBlockchainPrinter _printer;

    public string Header => "Blockchain";

    public BlockchainMenu(IBlockchainPrinter printer) =>
        _printer = printer;

    // TODO: сделать вывод информации, что блоков нет
    public void Handle() =>
        _printer.PrintAllBlocks();
}
