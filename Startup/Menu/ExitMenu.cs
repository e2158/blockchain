namespace Startup.Menu;

public class ExitMenu : IMenu
{
    public string Header => "Exit";

    public void Handle() => Environment.Exit(0);
}
