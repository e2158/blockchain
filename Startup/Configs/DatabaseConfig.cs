namespace Startup.Configs;

[Serializable]
public readonly struct DatabaseConfig
{
    public readonly string DatabaseName;
    public readonly string BlockCollectionName;
    public readonly string TransactionCollectionName;

    public DatabaseConfig(string databaseName, string blockCollectionName, string transactionCollectionName)
    {
        DatabaseName = databaseName;
        BlockCollectionName = blockCollectionName;
        TransactionCollectionName = transactionCollectionName;
    }
}
