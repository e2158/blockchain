using Blockchain;

namespace Startup.Configs;

[Serializable]
public readonly struct GenesisBlock
{
    public readonly int Height;
    public readonly string Creator;
    public readonly long Timestamp;
    public readonly string Hash;
    public readonly string PreviousHash;
    public readonly Transaction[] Transactions;

    public GenesisBlock(int height, string creator, long timestamp,
        string hash, string previousHash, Transaction[] transactions)
    {
        Height = height;
        Creator = creator;
        Timestamp = timestamp;
        Hash = hash;
        PreviousHash = previousHash;
        Transactions = transactions;
    }
}
