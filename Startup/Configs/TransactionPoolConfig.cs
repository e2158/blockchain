namespace Startup.Configs;

[Serializable]
public readonly struct TransactionPoolConfig
{
    public readonly int PoolCapacity;

    public TransactionPoolConfig(int poolCapacity) =>
        PoolCapacity = poolCapacity;
}
