namespace Startup.Configs;

[Serializable]
public readonly struct DividerWriterConfig
{
    public readonly string Divider;

    public DividerWriterConfig(string divider) =>
        Divider = divider;
}
