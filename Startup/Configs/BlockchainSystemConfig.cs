using Blockchain;

namespace Startup.Configs;

public class BlockchainSystemConfig
{
    public readonly DatabaseConfig DatabaseConfig = new("database", "blocks", "transactions");

    public readonly TransactionPoolConfig TransactionPoolConfig = new(5);

    public readonly TransactionValidatorConfig TransactionValidatorConfig = new(0.5f);

    public readonly DividerWriterConfig DividerWriterConfig = new("=========================");

    public readonly GenesisBlock GenesisBlock = new
    (
        height: 0,
        creator: "Admin",
        timestamp: DateTime.MinValue.Ticks,
        hash: "91727E71BEB8069D93A9F92F1C868FEFFA68F788626894F2A1832680FE4CF2DA",
        previousHash: "",
        transactions: new Transaction[]
        {
            new
            (
                timestamp: DateTime.MinValue.Ticks,
                sender: "System",
                recipient: "Genesis Account",
                amount: 0,
                fee: 0
            )
        }
    );
}
