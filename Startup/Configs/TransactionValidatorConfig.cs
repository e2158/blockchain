namespace Startup.Configs;

[Serializable]
public readonly struct TransactionValidatorConfig
{
    public readonly float MaxAmountRatioForFee;

    public TransactionValidatorConfig(float maxAmountRatioForFee) =>
        MaxAmountRatioForFee = maxAmountRatioForFee;
}
