using Input;
using Output;
using Zenject;
using Startup.Menu;
using Startup.Configs;
using Startup.Menu.Tools;

namespace Startup.Installers;

public class MenuInstaller : Installer
{
    public override void InstallBindings()
    {
        DiContainer subContainer = Container.CreateSubContainer();

        subContainer.Bind<IInput>().To<Input.Input>().AsCached();
        subContainer.Bind<IOutput>().To<Output.Output>().AsCached();
        subContainer.Bind<IDividerWriter>().To<DividerWriter>().AsCached();

        var dividerConfig = Container.Resolve<DividerWriterConfig>();
        subContainer.Bind<string>()
            .FromInstance(dividerConfig.Divider).WhenInjectedInto<DividerWriter>();

        subContainer.Bind<IMenu>().To<GenesisBlockMenu>().AsCached();
        subContainer.Bind<IMenu>().To<LastBlockMenu>().AsCached();
        subContainer.Bind<IMenu>().To<SendCoinMenu>().AsCached();
        subContainer.Bind<IMenu>().To<CreateBlockMenu>().AsCached();
        subContainer.Bind<IMenu>().To<AccountBalanceMenu>().AsCached();
        subContainer.Bind<IMenu>().To<TransactionHistoryMenu>().AsCached();
        subContainer.Bind<IMenu>().To<BlockchainMenu>().AsCached();
        subContainer.Bind<IMenu>().To<ExitMenu>().AsCached();
        subContainer.Bind<MainMenu>().AsCached();

        IMenu mainMenu = subContainer.Resolve<MainMenu>();
        Container.Bind<IMenu>().FromInstance(mainMenu).AsSingle();
    }
}
