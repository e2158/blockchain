using Utils;
using Zenject;
using Logging;
using Blockchain;

namespace Startup.Installers;

public class BlockInstaller : Installer
{
    public override void InstallBindings()
    {
        var loggerPool = Container.Resolve<ILoggerPool>();
        Container.Bind<ILogger>()
            .FromInstance(loggerPool.Get<BlockFactory>())
            .WhenInjectedInto<BlockFactory>();

        Container.Bind<IHashGenerator>().To<HashGenerator>().AsCached();
        Container.Bind<IBlockFactory>().To<BlockFactory>().AsSingle();
    }
}
