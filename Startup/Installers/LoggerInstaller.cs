using Output;
using Zenject;
using Logging;

namespace Startup.Installers;

public class LoggerInstaller : Installer
{
    public override void InstallBindings()
    {
        Container.Bind<IOutput>().To<ColoredOutput>()
            .WithArguments(ConsoleColor.Yellow)
            .WhenInjectedInto<ILoggerFactory>();

        Container.Bind<ILoggerFactory>().To<LoggerFactory>().AsCached();
        Container.Bind<ILoggerPool>().To<LoggerPool>().AsSingle();
    }
}
