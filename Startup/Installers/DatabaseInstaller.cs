using Zenject;
using Database;
using Blockchain;
using Startup.Configs;

namespace Startup.Installers;

public class DatabaseInstaller : Installer
{
    public override void InstallBindings()
    {
        var config = Container.Resolve<DatabaseConfig>();

        Container.Bind<IDb>().To<Db>().AsSingle().WithArguments(config.DatabaseName);

        var db = Container.Resolve<IDb>();
        IDbCollection<Block> blockCollection = db.GetCollection<Block>(config.BlockCollectionName);
        Container.Bind<IDbCollection<Block>>().FromInstance(blockCollection).AsSingle();

        IDbCollection<Transaction> transactionCollection = db.GetCollection<Transaction>(config.TransactionCollectionName);
        Container.Bind<IDbCollection<Transaction>>().FromInstance(transactionCollection).AsSingle();
    }
}
