using Zenject;
using Logging;
using Blockchain;
using Startup.Initialization;

namespace Startup.Installers;

public class BlockchainInstaller : Installer
{
    public override void InstallBindings()
    {
        var loggerPool = Container.Resolve<ILoggerPool>();

        Container.Bind<ILogger>()
            .FromInstance(loggerPool.Get<Blockchain.Blockchain>())
            .WhenInjectedInto<Blockchain.Blockchain>();

        Container.Bind<ICalculator>().To<Calculator>().AsSingle();
        Container.Bind<IBlockchain>().To<Blockchain.Blockchain>().AsSingle();
        Container.Bind<IBlockchainInitializer>().To<BlockchainInitializer>().AsSingle();
    }
}
