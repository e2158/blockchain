using Zenject;
using Serialization;

namespace Startup.Installers;

public class SerializationInstaller : Installer
{
    public override void InstallBindings() =>
        Container.Bind<ISerializer>().To<Serializer>().AsSingle();
}
