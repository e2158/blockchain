using Output;
using Zenject;
using Blockchain.Printing;

namespace Startup.Installers;

public class PrinterInstaller : Installer
{
    public override void InstallBindings()
    {
        var output = new Output.Output();
        Container.Bind<IOutput>().FromInstance(output).WhenInjectedInto<TransactionPrinter>();
        Container.Bind<IOutput>().FromInstance(output).WhenInjectedInto<BlockchainPrinter>();
        Container.Bind<IOutput>().FromInstance(output).WhenInjectedInto<BlockPrinter>();

        Container.Bind<ITransactionPrinter>().To<TransactionPrinter>().AsSingle();
        Container.Bind<IBlockchainPrinter>().To<BlockchainPrinter>().AsSingle();
        Container.Bind<IBlockPrinter>().To<BlockPrinter>().AsSingle();
    }
}
