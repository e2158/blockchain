using Zenject;
using Startup.Configs;

namespace Startup.Installers;

public class ConfigInstaller : Installer
{
    public override void InstallBindings()
    {
        var config = new BlockchainSystemConfig();

        Container.Bind<GenesisBlock>().FromInstance(config.GenesisBlock).AsSingle();
        Container.Bind<DatabaseConfig>().FromInstance(config.DatabaseConfig).AsSingle();
        Container.Bind<DividerWriterConfig>().FromInstance(config.DividerWriterConfig).AsSingle();
        Container.Bind<TransactionPoolConfig>().FromInstance(config.TransactionPoolConfig).AsSingle();
        Container.Bind<TransactionValidatorConfig>().FromInstance(config.TransactionValidatorConfig).AsSingle();
    }
}
