using Zenject;
using Logging;
using Blockchain;
using Startup.Configs;

namespace Startup.Installers;

public class TransactionInstaller : Installer
{
    public override void InstallBindings()
    {
        var poolConfig = Container.Resolve<TransactionPoolConfig>();
        Container.Bind<int>()
            .FromInstance(poolConfig.PoolCapacity)
            .WhenInjectedInto<TransactionPool>();

        var loggerPool = Container.Resolve<ILoggerPool>();
        Container.Bind<ILogger>()
            .FromInstance(loggerPool.Get<TransactionPool>())
            .WhenInjectedInto<TransactionPool>();

        var validationConfig = Container.Resolve<TransactionValidatorConfig>();
        Container.Bind<float>()
            .FromInstance(validationConfig.MaxAmountRatioForFee)
            .WhenInjectedInto<TransactionValidator>();

        Container.BindInterfacesTo<TransactionPool>().AsSingle();
        Container.Bind<ITransactionFactory>().To<TransactionFactory>().AsSingle();
        Container.Bind<ITransactionValidator>().To<TransactionValidator>().AsSingle();
    }
}
