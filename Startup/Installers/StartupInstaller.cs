using Timing;
using Zenject;

namespace Startup.Installers;

public class StartupInstaller : Installer
{
    public override void InstallBindings()
    {
        Container.Bind<ITimeSource>().To<TimeSource>().AsSingle();

        Container.Install<ConfigInstaller>();
        Container.Install<SerializationInstaller>();
        Container.Install<DatabaseInstaller>();
        Container.Install<LoggerInstaller>();
        Container.Install<BlockchainInstaller>();
        Container.Install<TransactionInstaller>();
        Container.Install<BlockInstaller>();
        Container.Install<PrinterInstaller>();
        Container.Install<MenuInstaller>();
    }
}
