using Zenject;
using Blockchain;
using Startup.Menu;
using Startup.Installers;
using Startup.Initialization;

const string articleUrl = "https://putukusuma.medium.com/developing-simple-crypto-application-using-c-48258c2d4e45";

Console.WriteLine("Learning Blockchain Project");
Console.WriteLine($"Original article: {articleUrl}");

var container = new DiContainer();
container.Install<StartupInstaller>();

var blockchain = container.Resolve<IBlockchain>();
var initializer = container.Resolve<IBlockchainInitializer>();
initializer.Initialize(blockchain);

var menu = container.Resolve<IMenu>();
while(true)
    menu.Handle();
