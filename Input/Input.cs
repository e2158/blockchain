﻿namespace Input;

public interface IInput
{
    int? ReadInt();
    double? ReadDouble();
    string? ReadString();
    void ReadAnyKey();
}

public class Input : IInput
{
    public int? ReadInt() => int.TryParse(ReadString(), out int value) ? value : null;
    public double? ReadDouble() => double.TryParse(ReadString(), out double value) ? value : null;
    public string? ReadString() => Console.ReadLine();
    public void ReadAnyKey() => Console.ReadKey();
}
