using Timing;

namespace Blockchain;

public interface ITransactionFactory
{
    Transaction Create(string sender, string recipient, double amount, double fee);
}

public class TransactionFactory : ITransactionFactory
{
    private readonly ITimeSource _timeSource;

    public TransactionFactory(ITimeSource timeSource) =>
        _timeSource = timeSource;

    public Transaction Create(string sender, string recipient, double amount, double fee) =>
        new(
            _timeSource.CalcTimestamp(),
            sender,
            recipient,
            amount,
            fee
        );
}
