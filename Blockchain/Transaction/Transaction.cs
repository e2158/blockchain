namespace Blockchain;

public readonly struct Transaction
{
    public readonly long Timestamp;
    public readonly string Sender;
    public readonly string Recipient;
    public readonly double Amount;
    public readonly double Fee;

    public Transaction(long timestamp, string sender, string recipient, double amount, double fee)
    {
        Timestamp = timestamp;
        Sender = sender;
        Recipient = recipient;
        Amount = amount;
        Fee = fee;
    }

    public override string ToString() =>
        $"{nameof(Sender)}: {Sender}, " +
        $"{nameof(Recipient)}: {Recipient}, " +
        $"{nameof(Amount)}: {Amount}, " +
        $"{nameof(Fee)}: {Fee}, " +
        $"{nameof(Timestamp)}: {Timestamp}";
}
