using Database;
using Logging;

namespace Blockchain;

public interface ITransactionPool
{
    event Action FilledUp;
}

public interface IEnqueueableTransactionPool : ITransactionPool
{
    void EnqueueTransaction(Transaction transaction);
}

public interface IDequeueableTransactionPool : ITransactionPool
{
    IEnumerable<Transaction> DequeueTransactions();
}

public class TransactionPool : ITransactionPool, IEnqueueableTransactionPool, IDequeueableTransactionPool
{
    public event Action FilledUp;

    private readonly ILogger _logger;
    private readonly int _poolCapacity;
    private readonly IDbCollection<Transaction> _transactions;

    private bool IsFilledUp => _transactions.Count >= _poolCapacity;

    public TransactionPool(int poolCapacity, IDbCollection<Transaction> transactions, ILogger logger)
    {
        _poolCapacity = poolCapacity;
        _transactions = transactions;
        _logger = logger;
        FilledUp = null!;
    }

    public void EnqueueTransaction(Transaction transaction)
    {
        _transactions.Add(transaction);
        _logger.Log($"Enqueued transaction by {transaction.Sender}");

        if (IsFilledUp)
        {
            _logger.Log("Filled Up");
            FilledUp();
        }
    }

    public IEnumerable<Transaction> DequeueTransactions()
    {
        Transaction[] transactions = _transactions.All.ToArray();
        _transactions.RemoveAll();

        _logger.Log("Dequeued all transactions");
        return transactions;
    }
}
