namespace Blockchain;

[Serializable]
public readonly struct ValidationResult
{
    public readonly bool IsError;
    public readonly string Error;

    public ValidationResult(bool isError = false, string error = "")
    {
        IsError = isError;
        Error = error;
    }
}
