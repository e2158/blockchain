namespace Blockchain;

public interface ITransactionValidator
{
    ValidationResult Validate(string sender, double amount, double fee);
}

public class TransactionValidator : ITransactionValidator
{
    private readonly ICalculator _calculator;
    private readonly float _maxAmountRatioForFee;

    public TransactionValidator(ICalculator calculator, float maxAmountRatioForFee)
    {
        _calculator = calculator;
        _maxAmountRatioForFee = maxAmountRatioForFee;
    }

    public ValidationResult Validate(string sender, double amount, double fee)
    {
        if (fee > _maxAmountRatioForFee * amount)
            return new ValidationResult
            (
                true,
                $"The fee has to be less than {_maxAmountRatioForFee * 100}% of amount!"
            );

        double senderBalance = _calculator.CalcBalance(sender);

        if (amount + fee > senderBalance)
            return new ValidationResult
            (
                true,
                $"Sender ({sender}) doesn't have enough balance!"
            );

        return new ValidationResult();
    }
}
