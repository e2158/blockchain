using Logging;
using Database;

namespace Blockchain;

public interface IBlockchain
{
    int BlocksCount { get; }
    Block LastBlock { get; }
    Block GenesisBlock { get; }
    IEnumerable<Block> Blocks { get; }

    void AddBlock(Block block);
}

public class Blockchain : IBlockchain
{
    private readonly ILogger _logger;
    private readonly IDbCollection<Block> _blocks;

    public int BlocksCount => _blocks.Count;
    public Block LastBlock => _blocks.Last;
    public Block GenesisBlock => _blocks.First;
    public IEnumerable<Block> Blocks => _blocks.All;

    public Blockchain(ILogger logger, IDbCollection<Block> blocks)
    {
        _logger = logger;
        _blocks = blocks;
    }

    public void AddBlock(Block block)
    {
        _blocks.Add(block);
        _logger.Log($"Added block {block.Hash}");
    }
}
