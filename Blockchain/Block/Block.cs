namespace Blockchain;

[Serializable]
public class Block
{
    public int Height { get; }
    public string Creator { get; }
    public long Timestamp { get; }
    public string Hash { get; }
    public string PreviousHash { get; }
    public Transaction[] Transactions { get; }

    public Block(int height, string creator, long timestamp,
        string hash, string previousHash, Transaction[] transactions)
    {
        Height = height;
        Creator = creator;
        Timestamp = timestamp;
        Hash = hash;
        PreviousHash = previousHash;
        Transactions = transactions;
    }

    public override string ToString() =>
        $"{nameof(Height)}: {Height}, " +
        $"{nameof(Creator)}: {Creator}, " +
        $"{nameof(Timestamp)}: {Timestamp}, " +
        $"{nameof(Hash)}: {Hash}, " +
        $"{nameof(PreviousHash)}: {PreviousHash}, " +
        $"{nameof(Transactions)}: {string.Join(", ", Transactions)}";
}
