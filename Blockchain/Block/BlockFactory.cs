using Utils;
using Timing;
using Logging;

namespace Blockchain;

public interface IBlockFactory
{
    Block Create(int height, string creator, string previousHash, Transaction[] transactions);
}

public class BlockFactory : IBlockFactory
{
    private readonly ILogger _logger;
    private readonly ITimeSource _timeSource;
    private readonly IHashGenerator _hashGenerator;

    public BlockFactory(ILogger logger, ITimeSource timeSource, IHashGenerator hashGenerator)
    {
        _logger = logger;
        _timeSource = timeSource;
        _hashGenerator = hashGenerator;
    }

    public Block Create(int height, string creator, string previousHash, Transaction[] transactions)
    {
        long timestamp = _timeSource.CalcTimestamp();
        string hash = CalcHash(timestamp, previousHash, transactions);
        var block = new Block(height, creator, timestamp, hash, previousHash, transactions);

        _logger.Log($"Created block {hash}");
        return block;
    }

    private string CalcHash(long timestamp, string previousHash, Transaction[] transactions)
    {
        byte[] timestampBytes = ByteConverter.Convert(timestamp);
        byte[] previousHashBytes = ByteConverter.Convert(previousHash);
        byte[] transactionBytes = ByteConverter.Convert(transactions);
        byte[] bytes = ByteArrayExtensions.Concat(timestampBytes, previousHashBytes, transactionBytes);

        return _hashGenerator.Generate(bytes).ToHexString();
    }
}
