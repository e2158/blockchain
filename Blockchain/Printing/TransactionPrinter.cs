using Utils;
using Output;

namespace Blockchain.Printing;

public interface ITransactionPrinter : IPrinter<Transaction>
{
    void Print(Transaction item, string prefix);
}

public class TransactionPrinter : ITransactionPrinter
{
    private readonly IOutput _output;

    public TransactionPrinter(IOutput output) =>
        _output = output;

    public void Print(Transaction transaction) =>
        Print(transaction, "");

    public void Print(Transaction transaction, string prefix)
    {
        _output.WriteLine($"{prefix}{nameof(transaction.Timestamp)} : {transaction.Timestamp.ToDateTime()}");
        _output.WriteLine($"{prefix}{nameof(transaction.Sender)}    : {transaction.Sender}");
        _output.WriteLine($"{prefix}{nameof(transaction.Recipient)} : {transaction.Recipient}");
        _output.WriteLine($"{prefix}{nameof(transaction.Amount)}    : {transaction.Amount}");
        _output.WriteLine($"{prefix}{nameof(transaction.Fee)}       : {transaction.Fee}");
    }
}
