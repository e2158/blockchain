namespace Blockchain.Printing;

public interface IPrinter<in TItem>
{
    void Print(TItem item);
}
