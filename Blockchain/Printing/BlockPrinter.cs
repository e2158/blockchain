using Utils;
using Output;

namespace Blockchain.Printing;

public interface IBlockPrinter : IPrinter<Block>
{ }

public class BlockPrinter : IBlockPrinter
{
    private readonly IOutput _output;
    private readonly ITransactionPrinter _transactionPrinter;

    public BlockPrinter(IOutput output, ITransactionPrinter transactionPrinter)
    {
        _output = output;
        _transactionPrinter = transactionPrinter;
    }

    public void Print(Block block)
    {
        _output.WriteLine($"{nameof(block.Height)}       : {block.Height}");
        _output.WriteLine($"{nameof(block.Timestamp)}    : {block.Timestamp.ToDateTime()}");
        _output.WriteLine($"{nameof(block.PreviousHash)} : {block.PreviousHash}");
        _output.WriteLine($"{nameof(block.Hash)}         : {block.Hash}");
        _output.WriteLine($"{nameof(block.Creator)}      : {block.Creator}");
        _output.WriteLine($"{nameof(block.Transactions)} :");

        for (int index = 0; index < block.Transactions.Length; index++)
        {
            _output.WriteLine($"  {index}:");
            _transactionPrinter.Print(block.Transactions[index], "    ");
        }
    }
}
