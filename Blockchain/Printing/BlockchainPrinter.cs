using Output;

namespace Blockchain.Printing;

// TODO: сделать возращение строк
public interface IBlockchainPrinter : IPrinter<IBlockchain>
{
    void PrintAllBlocks();
    void PrintGenesisBlock();
    void PrintLastBlock();
    void PrintTransactionHistory(string name);
}

public class BlockchainPrinter : IBlockchainPrinter
{
    private readonly IOutput _output;
    private readonly IBlockchain _blockchain;
    private readonly IBlockPrinter _blockPrinter;
    private readonly ITransactionPrinter _transactionPrinter;

    public BlockchainPrinter(IOutput output, IBlockchain blockchain, IBlockPrinter blockPrinter, ITransactionPrinter transactionPrinter)
    {
        _output = output;
        _blockchain = blockchain;
        _blockPrinter = blockPrinter;
        _transactionPrinter = transactionPrinter;
    }

    public void PrintAllBlocks() =>
        Print(_blockchain);

    public void Print(IBlockchain blockchain)
    {
        int counter = 0;
        int totalCount = blockchain.BlocksCount;

        foreach (Block block in blockchain.Blocks)
        {
            PrintBlock(block);

            bool isLast = counter == totalCount - 1;
            if (!isLast)
                PrintDivider();

            counter++;
        }
    }

    public void PrintGenesisBlock() =>
        PrintBlock(_blockchain.GenesisBlock);

    public void PrintLastBlock() =>
        PrintBlock(_blockchain.LastBlock);

    public void PrintTransactionHistory(string name)
    {
        Transaction[] transactions = FindTransactions(name).ToArray();

        for (int i = 0; i < transactions.Length; i++)
        {
            bool isLast = i == transactions.Length - 1;
            Transaction transaction = transactions[i];

            _transactionPrinter.Print(transaction);

            if (!isLast)
                PrintDivider();
        }
    }

    private IEnumerable<Transaction> FindTransactions(string name) =>
        _blockchain.Blocks
            .SelectMany(b => b.Transactions)
            .Where(t => t.Sender == name || t.Recipient == name);

    private void PrintBlock(Block block) =>
        _blockPrinter.Print(block);

    private void PrintDivider() =>
        _output.WriteLine("--------------");
}
