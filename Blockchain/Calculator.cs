namespace Blockchain;

public interface ICalculator
{
    double CalcBalance(string name);
    double CalcSpending(string name);
    double CalcIncome(string name);
}

public class Calculator : ICalculator
{
    private readonly IBlockchain _blockchain;

    public Calculator(IBlockchain blockchain) =>
        _blockchain = blockchain;

    public double CalcBalance(string name) =>
        CalcIncome(name) - CalcSpending(name);

    public double CalcSpending(string name) =>
        _blockchain.Blocks
            .SelectMany(b => b.Transactions)
            .Where(t => t.Sender == name)
            .Sum(t => t.Amount + t.Fee);

    public double CalcIncome(string name) =>
        _blockchain.Blocks
            .SelectMany(b => b.Transactions)
            .Where(t => t.Recipient == name)
            .Sum(t => t.Amount);
}
