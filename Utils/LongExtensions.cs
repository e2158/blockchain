namespace Utils;

public static class LongExtensions
{
    public static DateTime ToDateTime(this long ticks) => new(ticks);
}
