using System.Text;
using Newtonsoft.Json;

namespace Utils;

public static class ByteConverter
{
    public static byte[] Convert(long value) => BitConverter.GetBytes(value);
    public static byte[] Convert(bool value) => BitConverter.GetBytes(value);
    public static byte[] Convert(char value) => BitConverter.GetBytes(value);
    public static byte[] Convert(double value) => BitConverter.GetBytes(value);
    public static byte[] Convert(float value) => BitConverter.GetBytes(value);
    public static byte[] Convert(int value) => BitConverter.GetBytes(value);
    public static byte[] Convert(short value) => BitConverter.GetBytes(value);
    public static byte[] Convert(uint value) => BitConverter.GetBytes(value);
    public static byte[] Convert(ulong value) => BitConverter.GetBytes(value);
    public static byte[] Convert(ushort value) => BitConverter.GetBytes(value);

    public static byte[] Convert(object value)
    {
        string json = JsonConvert.SerializeObject(value);
        return Encoding.UTF8.GetBytes(json);
    }
}
