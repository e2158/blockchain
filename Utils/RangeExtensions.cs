namespace Utils;

public static class RangeExtensions
{
    public static bool Contains(this Range range, int value) =>
        value >= range.Start.Value && value <= range.End.Value;
}
