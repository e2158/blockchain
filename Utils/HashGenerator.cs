using System.Security.Cryptography;

namespace Utils;

public interface IHashGenerator
{
    byte[] Generate(byte[] bytes);
}

public class HashGenerator : IHashGenerator
{
    public byte[] Generate(byte[] bytes)
    {
        using var sha = SHA256.Create();
        byte[] hash = sha.ComputeHash(bytes);

        return hash;
    }
}
