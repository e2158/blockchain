namespace Utils;

public static class ByteArrayExtensions
{
    public static string ToHexString(this byte[] bytes) =>
        Convert.ToHexString(bytes);

    public static byte[] Concat(params byte[][] arrays)
    {
        int totalLength = CalcTotalLength(arrays);
        byte[] concatResult = new byte[totalLength];

        int currentLength = 0;
        foreach (byte[] array in arrays)
        {
            Buffer.BlockCopy(array, 0, concatResult, currentLength, array.Length);
            currentLength += array.Length;
        }

        return concatResult;
    }

    public static int CalcTotalLength(params byte[][] arrays)
    {
        int length = 0;

        foreach (byte[] array in arrays)
            length += array.Length;

        return length;
    }
}
