﻿using Newtonsoft.Json;

namespace Serialization;

public interface ISerializer
{
    string Serialize<T>(T obj);
    T? Deserialize<T>(string json);
}

public class Serializer : ISerializer
{
    public string Serialize<T>(T obj) => JsonConvert.SerializeObject(obj);
    public T? Deserialize<T>(string json) => JsonConvert.DeserializeObject<T>(json);
}
