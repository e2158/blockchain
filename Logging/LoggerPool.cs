namespace Logging;

public interface ILoggerPool
{
    ILogger Get<THost>();
    ILogger Get(string host = "");
}

public class LoggerPool : ILoggerPool
{
    private readonly ILoggerFactory _factory;
    private readonly Dictionary<string, ILogger> _loggers = new();

    public LoggerPool(ILoggerFactory factory) =>
        _factory = factory;

    public ILogger Get<THost>() =>
        Get(typeof(THost).Name);

    public ILogger Get(string host = "")
    {
        string key = string.IsNullOrEmpty(host) ? "" : host;

        if (!_loggers.TryGetValue(key, out var logger))
            logger = _loggers[key] = _factory.Create(key);

        return logger;
    }
}
