namespace Logging;

public interface ILogger
{
    string Host { get; }
    void Log(string message);
}
