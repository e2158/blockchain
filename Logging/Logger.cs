using Output;

namespace Logging;

public class Logger : ILogger
{
    private readonly IOutput _output;
    public string Host { get; }

    public Logger(IOutput output, string host = "")
    {
        Host = host;
        _output = output;
    }

    public void Log(string message) =>
        _output.WriteLine(WrapMessage(message));

    private string WrapMessage(string message) =>
        string.IsNullOrEmpty(Host)
            ? message : $"[{Host}]: {message}";
}