using Output;

namespace Logging;

public interface ILoggerFactory
{
    ILogger Create(string host = "");
}

public class LoggerFactory : ILoggerFactory
{
    private readonly IOutput _output;

    public LoggerFactory(IOutput output) => _output = output;

    public ILogger Create(string host = "") => new Logger(_output, host);
}
