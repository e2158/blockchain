using Serialization;
using NUnit.Framework;
using FluentAssertions;

namespace Tests.Unit.Serialization;

[TestOf(typeof(Serializer))]
public class SerializerTests
{
    private const string TestingClassString =
        @"{""Int"":7,""Str"":""test"",""Float"":10.2,""Bool"":true,""SubClass"":{""Int"":0,""Str"":""sub-test"",""Float"":0.0,""Bool"":false,""SubClass"":null}}";

    private static readonly TestingClass testingClass = new()
    {
        Int = 7,
        Float = 10.2f,
        Bool = true,
        Str = "test",
        SubClass = new TestingClass
        {
            Str = "sub-test",
            Bool = false
        }
    };

    private ISerializer _serializer;

    [SetUp]
    public void Setup() =>
        _serializer = new Serializer();

    [TestCase(5, "5")]
    [TestCase("hello", @"""hello""")]
    [TestCase('t', @"""t""")]
    [TestCase(true, @"true")]
    [TestCase(new[] { 1, 2, 3, 4 }, "[1,2,3,4]")]
    public void WhenSerializeObject_ThenReturnExpectedString(object obj, string expectedString)
    {
        // act
        string returnedString = _serializer.Serialize(obj);

        // assert
        returnedString.Should().Be(expectedString);
    }

    [Test]
    public void WhenSerializeTestingClass_ThenReturnCorrectString()
    {
        // act
        string returnedString = _serializer.Serialize(testingClass);

        // assert
        returnedString.Should().Be(TestingClassString);
    }

    [TestCase("5", 5)]
    [TestCase("2", 2)]
    [TestCase("-3", -3)]
    [TestCase("2987", 2987)]
    public void WhenDeserializeStringOfInteger_ThenReturnThatInteger(string stringOfInteger, int thatInteger) =>
        WhenDeserializeStringOfObject_ThenReturnThatObject(stringOfInteger, thatInteger);

    [TestCase("10.5", 10.5f)]
    [TestCase("2", 2f)]
    [TestCase("-3", -3f)]
    [TestCase("-109.07", -109.07f)]
    public void WhenDeserializeStringOfFloat_ThenReturnThatFloat(string stringOfFloat, float thatFloat) =>
        WhenDeserializeStringOfObject_ThenReturnThatObject(stringOfFloat, thatFloat);

    [TestCase(@"""c""", 'c')]
    [TestCase(@"""5""", '5')]
    public void WhenDeserializeStringOfChar_ThenReturnThatChar(string stringOfChar, char thatChar) =>
        WhenDeserializeStringOfObject_ThenReturnThatObject(stringOfChar, thatChar);

    [TestCase("true", true)]
    [TestCase("false", false)]
    public void WhenDeserializeStringOfBool_ThenReturnThatBool(string stringOfBool, bool thatBool) =>
        WhenDeserializeStringOfObject_ThenReturnThatObject(stringOfBool, thatBool);

    [TestCase("[1,2,-6,18]", new[] { 1, 2, -6, 18 })]
    public void WhenDeserializeStringOfArray_ThenReturnThatArray(string stringOfArray, int[] thatArray) =>
        WhenDeserializeStringOfObject_ThenReturnThatObject(stringOfArray, thatArray);

    [Test]
    public void WhenDeserializeTestingClass_ThenReturnEquivalentObject() =>
        WhenDeserializeStringOfObject_ThenReturnThatObject(TestingClassString, testingClass);

    private void WhenDeserializeStringOfObject_ThenReturnThatObject<TObject>(string stringOfObject, TObject thatObject)
    {
        // act
        var deserializedObject = _serializer.Deserialize<TObject>(stringOfObject)!;

        // assert
        deserializedObject.Should().BeEquivalentTo(thatObject);
    }
}
