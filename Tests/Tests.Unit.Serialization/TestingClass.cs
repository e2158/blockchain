using System;

namespace Tests.Unit.Serialization;

[Serializable]
internal class TestingClass
{
    public int Int;
    public string Str;
    public float Float;
    public bool Bool;
    public TestingClass SubClass;
}
