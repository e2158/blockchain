using Timing;
using NUnit.Framework;
using FluentAssertions;

namespace Tests.Unit.Timing;

[TestOf(typeof(TimeSource))]
public class TimeSourceTests
{
    private ITimeSource _timeSource;

    [SetUp]
    public void Setup() =>
        _timeSource = new TimeSource();

    [Test]
    public void WhenCalcTimestamp_AndCalcTimestampAgain_ThenSecondOneShouldBeGreater()
    {
        // act
        long firstTimestamp = _timeSource.CalcTimestamp();
        long secondTimestamp = _timeSource.CalcTimestamp();

        // assert
        secondTimestamp.Should().BeGreaterThan(firstTimestamp);
    }
}
