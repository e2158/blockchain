using Utils;
using NUnit.Framework;
using FluentAssertions;

namespace Tests.Unit.Utils;

[TestOf(typeof(HashGenerator))]
public class HashGeneratorTests
{
    private IHashGenerator _hashGenerator;

    [SetUp]
    public void SetUp() =>
        _hashGenerator = new HashGenerator();

    [TestCase(new byte[] { }, new byte[] { 0xE3, 0xB0, 0xC4, 0x42, 0x98, 0xFC, 0x1C, 0x14, 0x9A, 0xFB, 0xF4, 0xC8, 0x99, 0x6F, 0xB9, 0x24, 0x27, 0xAE, 0x41, 0xE4, 0x64, 0x9B, 0x93, 0x4C, 0xA4, 0x95, 0x99, 0x1B, 0x78, 0x52, 0xB8, 0x55 })]
    [TestCase(new byte[] { 0xA8 }, new byte[] { 0x74, 0xE1, 0xAD, 0xE3, 0x20, 0xC6, 0x60, 0x75, 0x46, 0x8E, 0x17, 0xCF, 0xAB, 0x33, 0xF4, 0x1E, 0x8E, 0x0E, 0xAC, 0xA4, 0x5E, 0xDB, 0x6D, 0xD7, 0xB0, 0x86, 0xC4, 0x9A, 0x35, 0x8D, 0x2A, 0x69 })]
    [TestCase(new byte[] { 0xFF, 0x06, 0xBC }, new byte[] { 0x94, 0x07, 0x90, 0xCE, 0x1F, 0x59, 0x33, 0x87, 0x0C, 0x9D, 0xB4, 0x84, 0x8F, 0xF8, 0xAF, 0x2D, 0x01, 0xF5, 0x9A, 0x55, 0xCE, 0x8A, 0xDA, 0x37, 0x9E, 0xBE, 0xEF, 0xB8, 0xAB, 0x07, 0x3E, 0xD7 })]
    public void WhenGetBytes_ThenReturnExpectedHash(byte[] bytes, byte[] hash) =>
        _hashGenerator.Generate(bytes).Should().BeEquivalentTo(hash);
}
