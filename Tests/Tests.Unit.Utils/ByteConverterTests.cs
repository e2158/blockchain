using Utils;
using NUnit.Framework;
using FluentAssertions;

namespace Tests.Unit.Utils;

[TestOf(typeof(ByteConverter))]
public class ByteConverterTests
{
    [TestCase(long.MinValue, new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80 })]
    [TestCase(-25,           new byte[] { 0xE7, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF })]
    [TestCase(-1,            new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF })]
    [TestCase(1,             new byte[] { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
    [TestCase(12,            new byte[] { 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
    [TestCase(long.MaxValue, new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F })]
    public void WhenGetLongValue_ThenReturnExpectedBytes(long value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(ulong.MinValue, new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
    [TestCase((ulong)1,       new byte[] { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
    [TestCase((ulong)12,      new byte[] { 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
    [TestCase(ulong.MaxValue, new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF })]
    public void WhenGetULongValue_ThenReturnExpectedBytes(ulong value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(int.MinValue, new byte[] { 0x00, 0x00, 0x00, 0x80 })]
    [TestCase(-25,          new byte[] { 0xE7, 0xFF, 0xFF, 0xFF })]
    [TestCase(-1,           new byte[] { 0xFF, 0xFF, 0xFF, 0xFF })]
    [TestCase(1,            new byte[] { 0x01, 0x00, 0x00, 0x00 })]
    [TestCase(12,           new byte[] { 0x0C, 0x00, 0x00, 0x00 })]
    [TestCase(int.MaxValue, new byte[] { 0xFF, 0xFF, 0xFF, 0x7F })]
    public void WhenGetIntValue_ThenReturnExpectedBytes(int value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(uint.MinValue, new byte[] { 0x00, 0x00, 0x00, 0x00 })]
    [TestCase((uint)1,       new byte[] { 0x01, 0x00, 0x00, 0x00 })]
    [TestCase((uint)12,      new byte[] { 0x0C, 0x00, 0x00, 0x00 })]
    [TestCase(uint.MaxValue, new byte[] { 0xFF, 0xFF, 0xFF, 0xFF })]
    public void WhenGetUIntValue_ThenReturnExpectedBytes(uint value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(short.MinValue, new byte[] { 0x00, 0x80 })]
    [TestCase(-25,            new byte[] { 0xE7, 0xFF })]
    [TestCase(-1,             new byte[] { 0xFF, 0xFF })]
    [TestCase(1,              new byte[] { 0x01, 0x00 })]
    [TestCase(12,             new byte[] { 0x0C, 0x00 })]
    [TestCase(short.MaxValue, new byte[] { 0xFF, 0x7F })]
    public void WhenGetShortValue_ThenReturnExpectedBytes(short value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(ushort.MinValue, new byte[] { 0x00, 0x00 })]
    [TestCase((ushort)1,       new byte[] { 0x01, 0x00 })]
    [TestCase((ushort)12,      new byte[] { 0x0C, 0x00 })]
    [TestCase(ushort.MaxValue, new byte[] { 0xFF, 0xFF })]
    public void WhenGetUShortValue_ThenReturnExpectedBytes(ushort value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(float.MinValue, new byte[] { 0xFF, 0xFF, 0x7F, 0xFF })]
    [TestCase(-5.9f,          new byte[] { 0xCD, 0xCC, 0xBC, 0xC0 })]
    [TestCase(1.2f,           new byte[] { 0x9A, 0x99, 0x99, 0x3F })]
    [TestCase(12.07f,         new byte[] { 0xB8, 0x1E, 0x41, 0x41 })]
    [TestCase(float.MaxValue, new byte[] { 0xFF, 0xFF, 0x7F, 0x7F })]
    public void WhenGetFloatValue_ThenReturnExpectedBytes(float value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(double.MinValue, new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xEF, 0xFF })]
    [TestCase(-5.9f,           new byte[] { 0x00, 0x00, 0x00, 0xA0, 0x99, 0x99, 0x17, 0xC0 })]
    [TestCase(1.2f,            new byte[] { 0x00, 0x00, 0x00, 0x40, 0x33, 0x33, 0xF3, 0x3F })]
    [TestCase(12.07f,          new byte[] { 0x00, 0x00, 0x00, 0x00, 0xD7, 0x23, 0x28, 0x40 })]
    [TestCase(double.MaxValue, new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xEF, 0x7F })]
    public void WhenGetDoubleValue_ThenReturnExpectedBytes(double value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(char.MinValue, new byte[] { 0x00, 0x00 })]
    [TestCase(' ',           new byte[] { 0x20, 0x00 })]
    [TestCase('g',           new byte[] { 0x67, 0x00 })]
    [TestCase('\t',          new byte[] { 0x09, 0x00 })]
    [TestCase(char.MaxValue, new byte[] { 0xFF, 0xFF })]
    public void WhenGetCharValue_ThenReturnExpectedBytes(char value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(false, new byte[] { 0x00 })]
    [TestCase(true,  new byte[] { 0x01 })]
    public void WhenGetBoolValue_ThenReturnExpectedBytes(bool value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);

    [TestCase(-7,     new byte[] { 0x2D, 0x37 })]
    [TestCase('p',    new byte[] { 0x22, 0x70, 0x22 })]
    [TestCase(true,   new byte[] { 0x74, 0x72, 0x75, 0x65 })]
    [TestCase(5.87f,  new byte[] { 0x35, 0x2E, 0x38, 0x37 })]
    [TestCase("test", new byte[] { 0x22, 0x74, 0x65, 0x73, 0x74, 0x22 })]
    public void WhenGetObjectValue_ThenReturnExpectedBytes(object value, byte[] bytes) =>
        ByteConverter.Convert(value).Should().BeEquivalentTo(bytes);
}
