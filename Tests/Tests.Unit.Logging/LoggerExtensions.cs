using Logging;

namespace Tests.Unit.Logging;

internal static class LoggerExtensions
{
    public static bool HasHost(this ILogger logger) => !string.IsNullOrEmpty(logger.Host);
}
