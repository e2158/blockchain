using NSubstitute;
using NUnit.Framework;
using FluentAssertions;
using System.Linq;
using System.Collections.Generic;
using Output;
using Logging;

namespace Tests.Unit.Logging;

[TestOf(typeof(LoggerFactory))]
public class LoggerFactoryTests
{
    private ILoggerFactory _loggerFactory;

    [SetUp]
    public void SetUp()
    {
        var output = Substitute.For<IOutput>();
        _loggerFactory = new LoggerFactory(output);
    }

    [Test]
    public void WhenCreateLogger_ThenLoggerIsNotNull()
    {
        // act
        ILogger logger = _loggerFactory.Create();

        // assert
        logger.Should().NotBeNull();
    }

    [TestCase(1)]
    [TestCase(2)]
    [TestCase(3)]
    [TestCase(8)]
    [TestCase(10)]
    public void WhenCreateManyLoggers_ThenAllLoggersAreDifferent(int loggersCount)
    {
        // act
        IEnumerable<ILogger> loggers =
            Enumerable.Range(0, loggersCount)
            .Select(_ => _loggerFactory.Create());

        IEnumerable<ILogger> uniqueLoggers = loggers.Distinct();
        int uniqueLoggersCount = uniqueLoggers.Count();

        // assert
        uniqueLoggersCount.Should().Be(loggersCount);
    }

    [TestCase(null)]
    [TestCase("")]
    [TestCase("host")]
    [TestCase("test host")]
    [TestCase("aaaaaaaaaa")]
    public void WhenCreateWithHostArgument_ThenLoggerHasThatHost(string host)
    {
        // act
        ILogger logger = _loggerFactory.Create(host);

        // assert
        logger.Host.Should().Be(host);
    }
}
