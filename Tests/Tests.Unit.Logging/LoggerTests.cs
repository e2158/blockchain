using Output;
using Logging;
using NSubstitute;
using NUnit.Framework;
using FluentAssertions;

namespace Tests.Unit.Logging;

[TestOf(typeof(Logger))]
public class LoggerTests
{
    private string _loggedString;
    private IOutput _output;

    [SetUp]
    public void SetUp()
    {
        _loggedString = string.Empty;

        _output = Substitute.For<IOutput>();
        _output
            .When(o => o.WriteLine(Arg.Any<string>()))
            .Do(x => _loggedString = x.Arg<string>());
    }

    [Test]
    public void WhenDoesNotGetHostNameAsConstructorArgument_ThenLoggerHasNotHost()
    {
        // arrange
        var logger = new Logger(_output);

        // assert
        logger.HasHost().Should().BeFalse();
    }

    [TestCase("")]
    [TestCase(null)]
    public void WhenHostNameIsNullOrEmpty_ThenLoggerIsNonHosted(string hostName)
    {
        // arrange
        var logger = new Logger(_output, hostName);

        // assert
        logger.HasHost().Should().BeFalse();
    }

    [TestCase(null)]
    [TestCase("")]
    [TestCase("host")]
    [TestCase("double host")]
    public void WhenGetHostNameAsConstructorArgument_ThenLoggerUsesThatHost(string hostName)
    {
        // arrange
        var logger = new Logger(_output, hostName);

        // assert
        logger.Host.Should().Be(hostName);
    }

    [TestCase("", "")]
    [TestCase("word", "word")]
    [TestCase("two words", "two words")]
    [TestCase("three small words", "three small words")]
    public void NonHostedLogger_WhenGetString_ThenLogThatString(string inputString, string expectedString)
    {
        // arrange
        var logger = new Logger(_output);

        // act
        logger.Log(inputString);

        // assert
        _loggedString.Should().Be(expectedString);
    }

    [TestCase("host", "", "[host]: ")]
    [TestCase("host", "word", "[host]: word")]
    [TestCase("big host", "word", "[big host]: word")]
    [TestCase("host", "two words", "[host]: two words")]
    public void HostedLogger_WhenGetString_ThenLogThatStringWithHostSign(string host, string inputString, string expectedString)
    {
        // arrange
        var logger = new Logger(_output, host);

        // act
        logger.Log(inputString);

        // assert
        _loggedString.Should().Be(expectedString);
    }
}
