using NSubstitute;
using NUnit.Framework;
using FluentAssertions;
using System.Linq;
using System.Collections.Generic;
using Output;
using Logging;

namespace Tests.Unit.Logging;

[TestOf(typeof(LoggerPool))]
public class LoggerPoolTests
{
    private ILoggerPool _loggerPool;

    [SetUp]
    public void SetUp()
    {
        var output = Substitute.For<IOutput>();
        var factory = new LoggerFactory(output);
        _loggerPool = new LoggerPool(factory);
    }

    [Test]
    public void WhenGetLogger_ThenLoggerIsNotNull()
    {
        // act
        ILogger logger = _loggerPool.Get();

        // assert
        logger.Should().NotBeNull();
    }

    [TestCase("host")]
    [TestCase("test host")]
    public void WhenGetLoggerViaHostName_ThenLoggerHasThatHostName(string hostName)
    {
        // act
        ILogger logger = _loggerPool.Get(hostName);

        // assert
        logger.Host.Should().Be(hostName);
    }

    [TestCase(null)]
    [TestCase("")]
    public void WhenGetLoggerViaNullOrEmptyHostName_ThenLoggerHasEmptyHostName(string hostName)
    {
        // act
        ILogger logger = _loggerPool.Get(hostName);

        // assert
        logger.Host.Should().Be("");
    }

    [Test]
    public void WhenGetLoggerViaGenericHost_ThenLoggerHasRelevantHostName()
    {
        // act
        ILogger logger = _loggerPool.Get<ILogger>();

        // assert
        logger.Host.Should().Be(nameof(ILogger));
    }

    [TestCase(1)]
    [TestCase(2)]
    [TestCase(5)]
    [TestCase(10)]
    public void WhenGetManyLoggerForOneHost_ThenAllLoggersAreTheSame(int loggersCount)
    {
        // act
        IEnumerable<ILogger> loggers =
            Enumerable.Range(0, loggersCount)
            .Select(_ => _loggerPool.Get("default"));

        IEnumerable<ILogger> uniqueLoggers = loggers.Distinct();
        int uniqueLoggersCount = uniqueLoggers.Count();

        // assert
        uniqueLoggersCount.Should().Be(1);
    }

    [TestCase(1)]
    [TestCase(2)]
    [TestCase(5)]
    [TestCase(10)]
    public void WhenGetManyLoggerWithoutHost_ThenAllLoggersAreTheSame(int loggersCount)
    {
        // act
        IEnumerable<ILogger> loggers =
            Enumerable.Range(0, loggersCount)
            .Select(_ => _loggerPool.Get());

        IEnumerable<ILogger> uniqueLoggers = loggers.Distinct();
        int uniqueLoggersCount = uniqueLoggers.Count();

        // assert
        uniqueLoggersCount.Should().Be(1);
    }

    [TestCase("host1")]
    [TestCase("host1", "host2")]
    [TestCase("host1", "host2", "host3")]
    [TestCase("host1", "host2", "host3", "host4")]
    public void WhenGetManyLoggerWithDifferentHosts_ThenAllLoggersAreDifferent(params string[] hostNames)
    {
        // act
        IEnumerable<ILogger> loggers = hostNames
            .Select(hostName => _loggerPool.Get(hostName));

        IEnumerable<ILogger> uniqueLoggers = loggers.Distinct();
        int uniqueLoggersCount = uniqueLoggers.Count();

        // assert
        uniqueLoggersCount.Should().Be(hostNames.Length);
    }

    [Test]
    public void WhenGetLoggerWithNullHost_AndGetLoggerWithEmptyHost_ThenAllLoggersAreTheSame()
    {
        // act
        ILogger nullHostLogger = _loggerPool.Get(null);
        ILogger emptyHostLogger = _loggerPool.Get("");

        // assert
        nullHostLogger.Should().Be(emptyHostLogger);
    }
}
