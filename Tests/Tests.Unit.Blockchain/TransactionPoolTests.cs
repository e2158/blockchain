using NSubstitute;
using NUnit.Framework;
using FluentAssertions;
using Logging;
using Database;
using Blockchain;
using System.Linq;
using System.Collections.Generic;

namespace Tests.Unit.Blockchain;

[TestOf(typeof(TransactionPool))]
public class TransactionPoolTests
{
    private ILogger _logger;
    private IDbCollection<Transaction> _transactions;

    [SetUp]
    public void SetUp()
    {
        _logger = StubLogger();
        _transactions = MockDbCollection();
    }

    [TestCase(0)]
    [TestCase(1)]
    [TestCase(2)]
    [TestCase(5)]
    [TestCase(10)]
    public void WhenEnqueueTransactions_AndDequeueAll_ThenDequeuedCountEqualsEnqueued(int enqueuedCount)
    {
        // arrange
        TransactionPool transactionPool = CreateTransactionPool(enqueuedCount + 1);
        IEnumerable<Transaction> transactions = CreateTransactions(enqueuedCount);

        // act
        foreach (Transaction transaction in transactions)
            transactionPool.EnqueueTransaction(transaction);

        IEnumerable<Transaction> dequeuedTransactions =
            transactionPool.DequeueTransactions();

        // assert
        dequeuedTransactions.Count().Should().Be(enqueuedCount);
    }

    [TestCase(1)]
    [TestCase(2)]
    [TestCase(5)]
    [TestCase(10)]
    public void WhenPoolCapacityIsReached_ThenInvokeFilledUp(int poolCapacity)
    {
        // arrange
        bool filledUp = false;
        TransactionPool transactionPool = CreateTransactionPool(poolCapacity);
        transactionPool.FilledUp += () => filledUp = true;

        IEnumerable<Transaction> transactions = CreateTransactions(poolCapacity);

        // act
        foreach (Transaction transaction in transactions)
            transactionPool.EnqueueTransaction(transaction);

        // assert
        filledUp.Should().BeTrue();
    }

    private static ILogger StubLogger() =>
        Substitute.For<ILogger>();

    private static IDbCollection<Transaction> MockDbCollection()
    {
        var list = new List<Transaction>();
        var dbCollection = Substitute.For<IDbCollection<Transaction>>();

        dbCollection.Count.Returns(_ => list.Count);
        dbCollection.First.Returns(_ => list[0]);
        dbCollection.Last.Returns(_ => list[^1]);
        dbCollection.All.Returns(_ => list);

        dbCollection
            .When(x => x.Add(Arg.Any<Transaction>()))
            .Do(x => list.Add(x.Arg<Transaction>()));

        dbCollection
            .When(x => x.Add(Arg.Any<IEnumerable<Transaction>>()))
            .Do(x => list.AddRange(x.Arg<IEnumerable<Transaction>>()));

        dbCollection
            .When(x => x.RemoveAll())
            .Do(_ => list.Clear());

        return dbCollection;
    }

    private TransactionPool CreateTransactionPool(int poolCapacity) =>
        new TransactionPool(poolCapacity, _transactions, _logger);

    private static IEnumerable<Transaction> CreateTransactions(int count) =>
        Enumerable.Range(0, count)
            .Select(_ => new Transaction());
}
