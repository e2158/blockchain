using Serialization;
using System.Collections;

namespace Database;

public interface IDbCollection
{
    int Count { get; }
    void RemoveAll();
}

public interface IDbCollection<T> : IDbCollection
{
    T First { get; }
    T Last { get; }
    IEnumerable<T> All { get; }
    void Add(T entity);
    void Add(IEnumerable<T> entities);
}

internal class DbCollection<T> : IDbCollection<T>
{
    private readonly DbCollectionSaver _saver;
    private readonly List<T> _list;

    public int Count => _list.Count;
    public T First => _list[0];
    public T Last => _list[^1];
    public IEnumerable<T> All => _list;

    public DbCollection(string filepath, ISerializer serializer)
    {
        _saver = new DbCollectionSaver(filepath, serializer);
        _list = new List<T>(_saver.Load<T>() ?? Array.Empty<T>());
    }

    public void Add(T entity)
    {
        _list.Add(entity);
        _saver.Save(_list);
    }

    public void Add(IEnumerable<T> entities)
    {
        _list.AddRange(entities);
        _saver.Save(_list);
    }

    public void RemoveAll()
    {
        _list.Clear();
        _saver.Save(_list);
    }
}
