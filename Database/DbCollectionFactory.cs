using Serialization;

namespace Database;

internal class DbCollectionFactory
{
    private readonly DirectoryInfo _directory;
    private readonly ISerializer _serializer;

    public DbCollectionFactory(DirectoryInfo directory, ISerializer serializer)
    {
        _directory = directory;
        _serializer = serializer;
    }

    public IDbCollection<T> Create<T>(string collectionName) =>
        new DbCollection<T>(GetFullCollectionPath(collectionName), _serializer);

    private string GetFullCollectionPath(string collectionName) =>
        Path.Combine(_directory.FullName, collectionName);
}
