using Serialization;

namespace Database;

internal class DbCollectionSaver
{
    private readonly FileInfo _file;
    private readonly ISerializer _serializer;

    public DbCollectionSaver(string filepath, ISerializer serializer)
    {
        _file = new FileInfo(filepath);
        _serializer = serializer;
    }

    public void Save<T>(IEnumerable<T> collection)
    {
        string json = _serializer.Serialize(collection);

        StreamWriter streamWriter = _file.CreateText();
        streamWriter.Write(json);
        streamWriter.Close();
    }

    public T[]? Load<T>()
    {
        if (!_file.Exists)
            return Array.Empty<T>();

        StreamReader streamReader = _file.OpenText();
        string json = streamReader.ReadToEnd();
        streamReader.Close();

        return _serializer.Deserialize<T[]>(json);
    }
}
