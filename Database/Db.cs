﻿using Serialization;

namespace Database;

public interface IDb
{
    IDbCollection<T> GetCollection<T>(string collectionName);
}

public class Db : IDb
{
    private readonly DbCollectionFactory _factory;
    private readonly Dictionary<string, IDbCollection> _collections;

    public Db(string directoryPath, ISerializer serializer)
    {
        _collections = new Dictionary<string, IDbCollection>();

        var directory = new DirectoryInfo(directoryPath);
        if (!directory.Exists)
            directory.Create();

        _factory = new DbCollectionFactory(directory, serializer);
    }

    public IDbCollection<T> GetCollection<T>(string collectionName)
    {
        if (!_collections.ContainsKey(collectionName))
            _collections[collectionName] = _factory.Create<T>(collectionName);

        return (IDbCollection<T>)_collections[collectionName];
    }


}
