namespace Timing;

public interface ITimeSource
{
    long CalcTimestamp();
}

public class TimeSource : ITimeSource
{
    public long CalcTimestamp() => DateTime.Now.Ticks;
}
