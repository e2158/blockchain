namespace Output;

public interface IOutput
{
    void WriteLine();
    void WriteLine(string message);
    void Write(string message);
    void Clear();
}
