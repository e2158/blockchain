namespace Output;

public class ColoredOutput : IOutput
{
    private ConsoleColor _initialForegroundColor;

    public ConsoleColor ConsoleColor { get; }

    public ColoredOutput(ConsoleColor consoleColor) =>
        ConsoleColor = consoleColor;

    public void WriteLine() =>
        WriteLine(string.Empty);

    public void WriteLine(string message)
    {
        Setup();
        Console.WriteLine(message);
        Restore();
    }

    public void Write(string message)
    {
        Setup();
        Console.Write(message);
        Restore();
    }

    public void Clear() =>
        Console.Clear();

    private void Setup()
    {
        _initialForegroundColor = Console.ForegroundColor;
        Console.ForegroundColor = ConsoleColor;
    }

    private void Restore() =>
        Console.ForegroundColor = _initialForegroundColor;
}
