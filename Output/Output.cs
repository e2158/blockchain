namespace Output;

public class Output : IOutput
{
    public void WriteLine() =>
        WriteLine(string.Empty);

    public void WriteLine(string message) =>
        Console.WriteLine(message);

    public void Write(string message) =>
        Console.Write(message);

    public void Clear() =>
        Console.Clear();
}
